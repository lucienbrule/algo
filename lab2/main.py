#!/usr/local/bin/python3
import random
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import warnings
warnings.filterwarnings("ignore")

"""
Erdos Renyi Graph
n: number of vertices
p: a number in [0,1], which is the probability that each of the n(n-1)/2 edges is independently included in the graph.
"""

def make_random_graph(n,p):
    if(n <= 1):
        print ("n must be int() and [1,inf)")
    if(p < 0 or p >1):
        print("p must be in [0,1]")
    report = nx.Graph()
    for x in range(0,n):
        report.add_node(x)

    for i in range(0,n):
        for j in range(0,i):
            q = random.random()
            if(q < p):
                report.add_edge(i,j)
    return report
# G is a Graph
# t is a number
def explore(G,v):
    visited = [False for x in range(0,nx.number_of_nodes(G))]
    color =  ["white" for x in range(0,nx.number_of_nodes(G))]
    val_map = {}
    path = []
    # print(visited)
    # print(color)
    def explore_(G,v,path):
        print(visited)
        print(color)
        visited[v] = True
        color[v] = "yellow"
        neighbors = nx.neighbors(G,v)
        print("neighbors::" + str(neighbors))
        no_new_nodes = False
        for n in neighbors:
            if visited[n] == False:
                explore_(G,n,path)
            else:
                no_new_nodes = True
        print(no_new_nodes,v)
        path.append(v)
        return path
    longest_path = explore_(G,v,path)
    print("path :: " + str(longest_path))
    # generate cmap
    for i in range(0,len(color)):
        if(color[i] == "yellow"):
            val_map[i] = .75
        elif(color[i] == "white"):
            val_map[i]= .5
        else:
            val_map[i] = .25
    return val_map,len(longest_path)
    # print("we did this")

def does_contain_connected_vertices(G,t):
    print(G.nodes())
    nodes = G.nodes()
    for n in nodes:
        print(nx.neighbors(G,n))
def main():
    # print('hello world')
    print(random.random())
    n=10 # 10 nodes
    # m=40 # 20 edges
    target_node = 0
    probability = .2
    clist = []
    plist = []
    tlist = []
    number_graphs_to_gen = 200
    # for c in np.arange(0.2,3.0,0.2):
    #     probability = c / n
    #     number_with_more_than_t_verticies = 0
    #     for Gn in range(0,number_graphs_to_gen):
    #         G = make_random_graph(n,probability)
    #         val_map,length_longest_path = explore(G,target_node)
    #         print("length longest path:: " + str(length_longest_path))
    #         plist.append(probability)
    #         clist.append(c)
    #         if(length_longest_path > 30):
    #             number_with_more_than_t_verticies = number_with_more_than_t_verticies + 1
    #     tlist.append(number_with_more_than_t_verticies)
    # print("plist: " + str(plist))
    # print("clist: " + str(clist))
    # print("tlist: " + str(tlist))
    G = make_random_graph(n,probability)

    # plt.plot(tlist)
    # plt.axis(clist)
    # plt.show()
    # val_map[target_node] = .01


    # [print(x) for x in nx.dfs_edges(G,target_node )]

    val_map = {1: 1.0,
           2: 0.2,
           target_node: 0.5714285714285714}
    values = [val_map.get(node, 0.25) for node in G.nodes()]
    try:
        nx.draw(G, nlist=[range(5, n), range(5)],axisbg="black", cmap=plt.get_cmap('spring'), with_labels=True, font_weight='bold',node_color=values)
        plt.show()
    except(KeyboardInterrupt,SystemExit):
        exit()


if __name__ == '__main__':
    main()
