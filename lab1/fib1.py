#!/usr/local/bin/python3
"""
the first one should
implement the recursive algorithm (Page 3 of the textbook)
"""
import sys
def fib(n):
    if(n <= 1):
        return n
    return fib(n - 1) + fib(n - 2)
def main(n):
    print("fib(" + str(n) + "): "  + str(fib(n)))

if __name__ == '__main__':
    main(int(sys.argv[1]))
