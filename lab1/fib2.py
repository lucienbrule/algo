#!/usr/local/bin/python3
"""
the second one
 should implement the iterative algorithm (Page 4)
 should compute the n-th Fibonacci number one million (10^6) times
(although you only need to print it out once)
You'll see the reason for this once
you execute the program - it is so fast that it's hard to get a meaningful difference
in times for different n without repeating
the process many times!
"""

import sys
def fib(n):
    if(n <= 1):
        return n
    now = 1;
    before = 1;

    for i in range(2,n):
        temp = now;
        now += before;
        before = temp;
    return now;

def main(n):
    print("fib(" + str(n) + "): "  + str(fib(n)))

if __name__ == '__main__':
    main(int(sys.argv[1]))
